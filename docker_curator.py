import json
import requests
import urllib.parse
import signal
import time
import argparse
import logging

logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s]  %(message)s', level = logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument("--url_cadvisor", default="http://cadvisor:8080",
                    help="cadvisor url. Default: http://localhost:8080")
parser.add_argument("--url_elastic", default="http://elasticsearch:9200",
                    help="elasticsearch url. Default: http://localhost:9200")
parser.add_argument("-i", "--interval", type=int, default=40,
                    help="set send data interval. Default is 40.")
parser.add_argument("--cadvisor_attempts", default=10,
                    help="set count of reconnect attempts to cadvisor. Default is 10.")
parser.add_argument("--tba", default=5,
                    help="set time between reconnect attempts to cadvisor. Default is 5.")

args = parser.parse_args()

url_cadvisor_docker = urllib.parse.urljoin(args.url_cadvisor, 'api/v1.0/containers/docker/')

exit_pointer = False
exception_counter = 0

def sigterm_handler(signum, frame):
    global exit_pointer
    exit_pointer = True

signal.signal(signal.SIGTERM, sigterm_handler)
signal.signal(signal.SIGINT, sigterm_handler)

def get_container_ids():
    responce = requests.get(url_cadvisor_docker).json()
    container_ids = []
    for cont_id in responce['subcontainers']:
        container_ids.append(cont_id['name'].split('/')[2])
    return container_ids

def get_container_info(id):
    url_container = urllib.parse.urljoin(url_cadvisor_docker, id)
    responce = requests.get(url_container).json()
    return responce

while not exit_pointer:
    try:
        for id in get_container_ids():
            info = get_container_info(id)
            name = info['aliases'][0]
            data = {'name': name, 'stats': info['stats'][0]}
            url_elastic_to = urllib.parse.urljoin(args.url_elastic, 'docker_status/' + name)
            resp = requests.post(url_elastic_to, data=json.dumps(data))
            logging.info("elastic_responce: " + str(resp.content))
        exception_counter = 0
        for i in range(args.interval):
            if exit_pointer:
                break
            time.sleep(1)
    except requests.ConnectionError as ce:
        exception_counter += 1
        logging.warning("Attempt " + str(exception_counter) + ": " + str(ce))
        if exception_counter > args.cadvisor_attempts:
            logging.error("Failed connection. Attempts is exhausted. " + str(ce))
            break
        time.sleep(args.tba)
    except ValueError as ve:
        logging.warning("Bad JSON received from cadvisor!")
        exception_counter += 1
        if exception_counter > args.cadvisor_attempts:
            logging.error("Failed connection. Attempts is exhausted. " + str(ce))
            break
        time.sleep(args.tba)