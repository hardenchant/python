# Use an official Python runtime as a parent image
FROM python:3

# Set the working directory to /app
WORKDIR /app

ADD . /app

RUN pip install --no-cache-dir -r requirements.txt

# Copy the current directory contents into the container at /app

ENTRYPOINT ["python", "./docker_curator.py"]
